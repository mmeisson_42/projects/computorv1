#! /usr/bin/env python3

import re
import sys

FLOAT_PATTERN = r"([0-9]+(?:\.[0-9]+)?)"
INTEGER_PATTERN = r"([+-]?[0-9]+)"
SIGN_PATTERN = r"(?:\s*([+-])\s*)?"
SPACES = r"\s*"

pattern = r"(?:{sign_pattern}((?:(?:{float_pattern}{sp}\*)?{sp}[xX]{sp}\^{sp})?{integer_pattern}))".format(
    float_pattern=FLOAT_PATTERN,
    sp=SPACES,
    integer_pattern=INTEGER_PATTERN,
    sign_pattern=SIGN_PATTERN
        )

def sqrt(nb, precision=10):
    i = 0.0
    prec = 1.0
    for tmp in range(precision):
        while i*i < nb:
            i += prec;
        i -= prec
        prec *= 0.1
    return i


class Member:

    def __init__(self, sign='+', degree=None, quantity=None):
        if degree is not None:
            self.degree = degree
        else:
            self.degree = 0
        if quantity is not None:
            self.quantity = quantity
        else:
            self.quantity = 0
        if sign == '-':
            self.quantity *= -1

    def __add__(self, other):
        if self.degree != other.degree:
            raise ValueError("Adding members of different degrees: {} and {}".format(
                self.degree, other.degree
                ))
        return (Member(degree=self.degree, quantity=self.quantity + other.quantity))

    def __sub__(self, other):
        if self.degree != other.degree:
            raise ValueError("Substracting members of different degrees: {} and {}".format(
                self.degree, other.degree
                ))
        return (Member(degree=self.degree, quantity=self.quantity - other.quantity))

    def __truediv__(self, other):
        return self.quantity / other.quantity

    def __mul__(self, other):
        return self.quantity * other.quantity


def equation_degree(members):
    degree = 0
    for member in members:
        if members[member].quantity:
            degree = max(degree, members[member].degree)
    return degree


def solve_one(equation):
    print("a = {}\tb = {}\tc = {}".format(
        equation["a"].quantity,
        equation["b"].quantity,
        equation["c"].quantity,
        ))
    print("The solution is -(c / b)".format(
        equation["c"].quantity,
        equation["b"].quantity,
        ))
    res = -(equation["c"] / equation["b"])
    print("The solution is\n{}".format(res))


def solve_two(equation):
    print("a = {}\tb = {}\tc = {}".format(
        equation["a"].quantity,
        equation["b"].quantity,
        equation["c"].quantity,
        ))
    discriminant = equation["b"].quantity * equation["b"].quantity - \
            4.0 * equation["a"].quantity * equation["c"].quantity
    print("Discriminant found: {}".format(discriminant))
    if discriminant == 0:
        print("The solution is -b / (2.0 * a)")
        res = -equation["b"].quantity / (2.0 * equation["a"].quantity)
        print("The solution is:\n{}".format(res))
    elif discriminant > 0:
        print("The solutions are:\n"
                "\t-b - sqrt(discriminant) / (2.0 * a)"
                "\t-b + sqrt(discriminant) / (2.0 * a)"
                )
        res = []
        res.append((-equation["b"].quantity - sqrt(discriminant)) /
                (2.0 * equation["a"].quantity))
        res.append((-equation["b"].quantity + sqrt(discriminant)) /
                (2.0 * equation["a"].quantity))
        print("Discriminant is strictly positive ({}), the two solutions are:\n{}\n{}".format(
            discriminant,
            res[0],
            res[1]
            ))

def parse(equation):
    def split_chunks(equation, delims=" "):
        for chunk in [delims[0] + chunk for chunk in equation.split(delims[0])]:
            if len(delims) > 1:
                yield split_chunks(chunk, delims[1:])
            else:
                yield chunk

    matchs = []
    chunks = []
    for chunk in split_chunks(equation, delims="+-"):
        pass

    
def resolve(equation_as_string):
    equations = equation_as_string.split('=')
    for equation in equations:
        res = re.findall(r"(\^\s*[0-9]+\.)".format(float_pattern=FLOAT_PATTERN), equation)
        if res:
            raise IndexError
    matchs = {
            "left": re.findall(pattern, equations[0]),
            "right": re.findall(pattern, equations[1]),
        }
    for side in ("left", "right"):
        matchs[side] = [
                Member(
                    sign=member[0] or '+',
                    quantity=float(member[2] or member[3] or '0'),
                    degree=int(member[3] or '0' if member[2] else 0)
                ) for member in matchs[side]
            ]

    equation = {}
    a = 97
    for match in matchs["left"]:
        idx = chr(match.degree + a)
        equation[idx] = equation.get(idx, Member(degree=match.degree)) + match

    for match in matchs["right"]:
        idx = chr(match.degree + a)
        equation[idx] = equation.get(idx, Member(degree=match.degree)) - match
    
    degree = equation_degree(equation)
    reduced_form = []
    for i in range(degree + 2):
        idx = chr(i + a)
        try:
            if equation[idx].quantity:
                reduced_form.append("{} * x^{}".format(equation[idx].quantity, i))
        except KeyError:
            equation[idx] = Member(degree=i)
    reduced_form = " + ".join(reduced_form)

    equation["a"], equation["c"] = equation.get("c", Member(degree=0)), equation.get("a", Member(degree=2))

    print("Reduced form: {} = 0".format(reduced_form or "0"))
    print("Polynomial degree: {}".format(degree))
    if degree == 0:
        print("Solutions are every real numbers")
    elif degree == 1:
        solve_one(equation)
    elif degree == 2:
        solve_two(equation)
    else:
        print("The polynomial degree is stricly greater than 2, I can't solve.")



if __name__ == "__main__":
    prog = re.compile(pattern)
    try:
        resolve(sys.argv[1])
    except IndexError:
        print("Usage: {} 'equation'".format(sys.argv[0]))
        print("\t'equation' should be of the form 'n * x^l'+ = 'm * x^o'+")
